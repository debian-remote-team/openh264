#!/bin/bash

GMP_API_BRANCH=Firefox39
GTEST_VER=release-1.8.1

if [ ! -d gmp-api ] ; then git clone https://github.com/mozilla/gmp-api gmp-api ; fi
cd gmp-api && git fetch origin && git checkout $GMP_API_BRANCH && cd -

if [ ! -d gtest ] ; then git clone https://github.com/google/googletest.git gtest && \
cd gtest && git checkout -b $GTEST_VER $GTEST_VER ; fi

